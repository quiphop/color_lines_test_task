
export const rand = (min, max) => {
	return (Math.floor(Math.random() * (max - min +1 )) + min);
};

export const each = (obj, cb) => {
	for(let i = 0; i < obj.length; i++) {
		cb(obj[i], i);
	}
};
