import { CONFIG } from './config';

export class Astar {

	constructor(grid) {
		this.nodes = [];
		this.openset = [];
		this.grid = grid;
	}

	init(startX, startY) {

		for(let i = 0; i < CONFIG.GRID_SIZE; i++) {
			this.nodes[i] = [];

			for(let j = 0; j < CONFIG.GRID_SIZE; j++) {
				this.nodes[i][j] = {
					obstacle: this.grid[i][j],
					parent: 0,
					f:0,
					g:0,
					h:0,
					x:j,
					y:i,
					closed: false
				};
			}
		}
		// Adds start node to the openset
		this.openset.push(this.nodes[startY][startX]);
	}

	find(startX, startY, endX, endY) {
		this.init(startX, startY);

		// Goes through all open nodes
		while(this.openset.length) {

			let index = 0;

			// Finds the node index with the highest F value
			for(let i = 0; i < this.openset.length; i++) {
				if(this.openset[i].f < this.openset[index].f) {
					index = i;
				}
			}

			const currentNode = this.openset[index];

			// Checks if the end node is reached
			if(currentNode.x == endX && currentNode.y == endY) {
				return this.reconstructPath(currentNode);
			}

			// Removes current node from openlist and sets it as closed
			this.openset.splice(index, 1);
			currentNode.closed = true;

			// Get all adjecent nodes
			const neighbors = this.getNeighbors(currentNode);
			for(let i = 0; i < neighbors.length; i++) {

				const neighbor = neighbors[i];

				// Checks if adjecent node is closed or it's not walkable
				if(neighbor.closed || neighbor.obstacle != 0){
					continue;
				}

				const g = currentNode.g + 1;
				let gIsBest = false;

				// Checks if node isn't opened yet
				if(!this.isOpened(neighbor)) {
					gIsBest = true;
					neighbor.h = Math.abs(neighbor.x - endX) + Math.abs(neighbor.y - endY);
					this.openset.push(neighbor);
				} else if(g < neighbor.g) {
					gIsBest = true;
				}

				if(gIsBest){
					neighbor.parent = currentNode;
					neighbor.g = g;
					neighbor.f = neighbor.g + neighbor.h;
				}
			}
		}

		// Path is not found
		return false;
	}


	reconstructPath(node) {
		const path = [];
		while(node.parent){
			path.push(node);
			node = node.parent;
		}

		return path.reverse();

	}

	getNeighbors(node) {
		const neighbors = [];
		const x = node.x;
		const y = node.y;

		if (y - 1 >= 0) neighbors.push(this.nodes[y - 1][x]);
		if (y + 1 <= 8) neighbors.push(this.nodes[y + 1][x]);

		if (x - 1 >= 0) neighbors.push(this.nodes[y][x - 1]);
		if (x + 1 <= 8) neighbors.push(this.nodes[y][x + 1]);

		return neighbors;
	}

	isOpened(node) {
		for(let i = 0; i < this.openset.length; i++) {
			if(this.openset[i].x == node.x && this.openset[i].y == node.y) {
				return true;
			}
		}
		return false;
	}
}
