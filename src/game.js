import { Astar } from './astar';
import { rand, each } from './utils';
import { COLORS } from './colors';
import { CONFIG } from './config';

export default class Game {

	constructor() {
		this.gridElement = document.querySelector('.grid');
		this.naxtBallsElement = document.querySelector('.next-balls');

		this.grid = [];
		this.gridBlocked = false;
		this.pickedBall = null;
		this.colors = COLORS;

		this.generateNextBalls();
		this.generateGrid();
	}

	addBalls(cb) {

		const cells = [];

		for(let i = 0; i < CONFIG.NEXT_BALLS_COUNT; i++) {

			const emptyCells = this.getCells('.empty');
			if (emptyCells.length > 0) {

				// Get random empty cell
				const cell = emptyCells[rand(0, emptyCells.length - 1)];
				this.grid[cell.dataset.y][cell.dataset.x] = this.colors.key(this.nextBalls[i]);

				cells.push(cell);
				cell.className = `ball ${this.nextBalls[i]}`;

			} else break;

		}

		this.generateNextBalls();
	
		if(cb) return cb(cells);

	}

	generateGrid() {
		for(let i = 0; i < CONFIG.GRID_SIZE; i++) {
			this.grid[i] = [];
			for(let j = 0; j < CONFIG.GRID_SIZE; j++) {

				this.grid[i][j] = 0;

				const cell = document.createElement('div');

				cell.id = `cell-${j}-${i}`;
				cell.className = 'empty';
				cell.dataset.x = j;
				cell.dataset.y = i;
				cell.style.left = `${(j * 50)}px`;
				cell.style.top = `${(i * 50)}px`;

				this.gridElement.appendChild(cell);

				cell.addEventListener('click', (e) => this.onCellClick(e));

			}
		}
		this.addBalls();
	}

	gameOver() {
		this.gridBlocked = true;
		alert('game over :(');
	}

	// Top-3 balls for the next turn
	generateNextBalls() {
		this.nextBalls = [];
		this.naxtBallsElement.innerHTML = '';

		for(let i = 0; i < CONFIG.NEXT_BALLS_COUNT; i++) {
			const ball = document.createElement('div');

			this.nextBalls[i] = this.colors[rand(1, 7)];
			ball.className = `ball ${this.nextBalls[i]}`;
			this.naxtBallsElement.appendChild(ball);
		}
	}

	getCell(x, y) {
		const cellId = `cell-${x}-${y}`;
		return document.getElementById(cellId);
	}

	getCells(selector) {
		return this.gridElement.querySelectorAll(selector);
	}

	// Tryin' to find the 5-or-more line vertical/horizontal/diagonal
	getLines(cell) {
		
		const x = parseInt(cell.dataset.x);
		const y = parseInt(cell.dataset.y);
		const ball = this.colors.key(cell.classList.item(1)); // Key color, int
		const lines = [
			[[x,y]], // Horizontal
			[[x,y]], // Vertical
			[[x,y]], // Diagonal (Left-up to Right-down)
			[[x,y]] // Diagonal (Right-up to Left-down)
		];

		// Left, Right, Down, Up, Left-up, Right-up, Left-down, Right-down
		let	l, r, d, u, lu, ru, ld, rd;
		l = r = d = u = lu = ru = ld = rd = ball;

		let i = 1;
		while([l, r, u, d, lu, ru, ld, rd].indexOf(ball) !== -1) {

			// Horizontal lines
			if (l === this.grid[y][x - i]) lines[0].push([x-i, y]);
			else l = -1;

			if (r === this.grid[y][x + i]) lines[0].push([x + i, y]);
			else r = -1;
			
			// Vertical lines
			if (y - i >= 0 && u === this.grid[y - i][x]) lines[1].push([x, y - i]);
			else u = -1;

			if (y + i <= 8 && d === this.grid[y + i][x]) lines[1].push([x, y + i]);
			else d = -1;

			// Diagonal lines
			if (y - i >= 0 && lu === this.grid[y - i][x - i]) lines[2].push([x - i, y - i]);
			else lu = -1;

			if (y + i <= 8 && rd === this.grid[y + i][x + i]) lines[2].push([x + i, y + i]);
			else rd = -1;

			if (y + i <= 8 && ld === this.grid[y + i][x - i]) lines[3].push([x - i, y + i]);
			else ld = -1;

			if (y - i >= 0 && ru === this.grid[y - i][x + i]) lines[3].push([x + i, y - i]);
			else ru = -1;

			i++;
		}

		// Line can't be desctructed if there less tnen 5 balls
		// reset counter
		for(let i = lines.length - 1; i >= 0; i--) {
			if(lines[i].length < 5) {
				lines.splice(i, 1);
			}
		}

		// Return an array of balls with positions
		return (lines.length > 0) ? lines : false;

	}

	moveBall(from, to, path, cb) {

		// To prevent picking balls while current ball is moving 
		this.gridBlocked = true;
		this.grid[from.dataset.y][from.dataset.x] = 0;

		const color = from.classList.item(1);
		let previous;

		from.className = 'empty';
		this.pickedBall = null;

		for(var i = 0; i <= path.length; i++) {
			((i) => {
				setTimeout(() => {

					if(path.length == i) {
						// Adds ball to destination cell
						this.grid[to.dataset.y][to.dataset.x] = this.colors.key(color);
						to.className = `ball ${color}`;
						this.gridBlocked = false;
						return cb();
					}
				
					if(previous) {
						previous.className = 'empty';
					}

					const cell = previous = this.getCell(path[i].x, path[i].y);
					cell.className = `ball ${color}`;

				}, 50 * i);
			})(i);
		}

	}

	onBallClick(e) {

		// Unselects previously selected cell
		each(this.getCells('.ball'), cell => {
			return cell.classList.remove('selected');
		});

		this.pickedBall = e.currentTarget;
		this.pickedBall.classList.add('selected');
	}

	onCellClick(e) {
		if(this.gridBlocked) {
			return;
		} else if(e.currentTarget.className === 'empty') {
			this.onEmptyCellClick(e);
		} else {
			this.onBallClick(e);
		}
	}


	onEmptyCellClick(e) {

		if(!this.pickedBall) {
			return;
		}
		
		const to  = e.currentTarget;
		const from = this.pickedBall;

		const astar = new Astar(this.grid);
		const path = astar.find(from.dataset.x, from.dataset.y, to.dataset.x, to.dataset.y);

		if (path) {
			this.moveBall(from, to, path, () => {

				const lines = this.getLines(to);
				
				// Checks if there are five--or-more-ball lines
				if (!lines) {
					this.addBalls((cells) => {
						const lineSets = [];

						for(let i = 0; i < cells.length; i++) {
							const lines = this.getLines(cells[i]);
							if(lines) lineSets.push(lines);
						}

						// Checks if five-ball lines are found after adding balls				
						if(lineSets.length > 0) {
							this.removeLines(lineSets);
						} else {
							// Game over if every cell is filled
							if(!this.getCells('.empty').length) return this.gameOver();
						}
					});
				} else {
					this.removeLines([lines]);
				}
			});
		}
	}

	removeLines(lineSets) {
		for(let k in lineSets) {

			const lines = lineSets[k];

			for(let i = 0; i < lines.length; i++) {
				for(let j = 0; j < lines[i].length; j++) {
					const x = lines[i][j][0];
					const y = lines[i][j][1];
					const cell = this.getCell(x, y);
					cell.className = 'empty';
					this.grid[y][x] = 0;
				}
			}
		}
	}
}
