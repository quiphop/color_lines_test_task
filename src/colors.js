export const COLORS = {
	1: 'red',
	2: 'orange',
	3: 'yellow',
	4: 'green',
	5: 'blue',
	6: 'lightblue',
	7: 'indigo',
	key: function(color) {
		for(var key in this) {
			if(this[key] === color) {
				return parseInt(key);
			}
		}
	}
};
